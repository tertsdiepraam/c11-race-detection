LIB=/home/vagrant/c11tester
CXX=LD_LIBRARY_PATH=${LIB} ./clang++
C11TESTER='-x1'

all: simplest test1 test2 test3 test4 philosophers thinair fence

build:
	mkdir build

simplest: test/simplest.cpp build
	$(CXX) test/$@.cpp -o build/$@

test1: test/test1.cpp build
	$(CXX) test/$@.cpp -o build/$@

test2: test/test2.cpp build
	$(CXX) test/$@.cpp -o build/$@

test3: test/test3.cpp build
	$(CXX) test/$@.cpp -o build/$@

test4: test/test4.cpp build
	$(CXX) test/$@.cpp -o build/$@

philosophers: test/philosophers.cpp build
	$(CXX) test/$@.cpp -o build/$@

thinair: test/thinair.cpp build
	$(CXX) test/$@.cpp -o build/$@

fence: test/fence.cpp build
	$(CXX) test/$@.cpp -o build/$@

increment: test/increment.cpp build
	$(CXX) test/$@.cpp -o build/$@

weird: test/weird.cpp build
	$(CXX) test/$@.cpp -o build/$@

