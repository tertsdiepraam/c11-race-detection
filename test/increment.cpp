#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0);

void t() {
  int r = x.load(memory_order_relaxed);
  x.store(r + 1, memory_order_relaxed);
}

int main() {
  thread a(t), b(t);
  a.join();
  b.join();
  printf("x = %d\n", x.load(memory_order_relaxed));
  return 0;
}
