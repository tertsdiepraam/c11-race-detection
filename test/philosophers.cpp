#include <iostream>
#include <mutex>
#include <thread>

#define P 5

using namespace std;

mutex m[P];

// Philosopher i needs forks (mutexes) i and i+1 mod P
void philosopher(int i) {
  int j = (i + 1) % P;
  m[i].lock();
  m[j].lock();
  cout << "eating" << endl;
  m[i].unlock();
  m[j].unlock();
}

int main() {
  thread threads[P];
  for (int i = 0; i < P; i++) {
    threads[i] = thread(philosopher, i);
  }
  for (auto &t : threads) {
    t.join();
  }
  return 0;
}
