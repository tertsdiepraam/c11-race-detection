#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0), y(0);

void A() {
  x.store(1, memory_order_relaxed);
  y.store(1, memory_order_relaxed);
}

void B() {
  int r1 = x.load(memory_order_relaxed);
  int r2 = y.load(memory_order_relaxed);
}

int main(int argc, char **argv) {
  thread t1(A), t2(B);

  t1.join();
  t2.join();

  return 0;
}
