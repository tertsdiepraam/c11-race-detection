#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0);

void a() {
    int v = x.load(memory_order_relaxed);
    x.store(v+1, memory_order_relaxed);
}

int main() {
    thread t1(a), t2(a);
    t1.join();
    t2.join();
    return 0;
}
