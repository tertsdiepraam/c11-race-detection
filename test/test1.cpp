// This program deadlocks
#include <iostream>
#include <mutex>

using namespace std;

mutex m1;

int main() {
  m1.lock();
  m1.lock();
}
