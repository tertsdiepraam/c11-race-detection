#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(-1), y(0);

void A() {
  int r1 = x.load(memory_order_relaxed);
  y.store(r1, memory_order_relaxed);
  printf("r1=%d\n", r1);
}

void B() {
  int r2 = y.load(memory_order_relaxed);
  x.store(r2, memory_order_relaxed);
  x.store(r2 + 1, memory_order_relaxed);
  printf("r2=%d\n", r2);
}

int main() {
  thread t1(A), t2(B);
  t1.join();
  t2.join();
  return 0;
}
