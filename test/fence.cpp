#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0);

void A() {
    atomic_thread_fence(memory_order_acquire);
    x.store(2, memory_order_relaxed);
    atomic_thread_fence(memory_order_release);
}

void B() {
    atomic_thread_fence(memory_order_acquire);
    x.load(memory_order_relaxed);
    atomic_thread_fence(memory_order_release);
}

int main() {
    thread a(A), b(B);
    a.join();
    b.join();
    return 0;
}
