#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0), y(0);

void A() {
  x.store(1, memory_order_relaxed);
  y.store(1, memory_order_relaxed);
}

void B() {
  int r1 = x.load(memory_order_relaxed);
  int r2 = y.load(memory_order_relaxed);
  printf("r1 = %d, r2 = %d\n", r1, r2);
}

void C() {
  int r2 = y.load(memory_order_relaxed);
  int r1 = x.load(memory_order_relaxed);
  printf("r1 = %d, r2 = %d\n", r1, r2);
}

int main(int argc, char **argv) {
  thread t1(A), t2(B), t3(C);

  t1.join();
  t2.join();
  t3.join();
  printf("Main thread is finished\n");

  return 0;
}
