
#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0), y(0);

void A() {
  x.store(1, memory_order_relaxed);
}

void B() {
  while (x.load(memory_order_relaxed) == 0) {}
  y.store(1, memory_order_relaxed);
}

void C() {
  while (y.load(memory_order_relaxed) == 0) {}
  if (x.load(memory_order_relaxed) == 0) {
    printf("Something weird happened");
  }
}

int main() {
  thread a(A), b(B), c(C);
  a.join();
  b.join();
  c.join();
  return 0;
}
