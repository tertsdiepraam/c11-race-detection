#include <atomic>
#include <stdio.h>
#include <thread>

using namespace std;

atomic<int> x(0), y(0);

void A() {
  int r1 = x.load(memory_order_acquire);
  int r2 = x.load(memory_order_relaxed);
  int r3 = y.load(memory_order_relaxed);
  printf(" r1 = %d\n", r1);
  printf(" r2 = %d\n", r2);
}

void B() {
  x.store(1, memory_order_release);
}

int main() {
  thread a(A), b(B);
  a.join();
  b.join();
  return 0;
}
