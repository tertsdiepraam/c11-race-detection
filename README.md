# C11 Race Detection
By Terts Diepraam

## Installation

This project uses c11tester, which can be
[downloaded as an artifact](https://dl.acm.org/do/10.1145/3410278/full/).
Follow the installation instructions of that artifact to get it up and running.

The minimum supported Python version is 3.6.

## Configuration

This folder needs to be mounted on the Vagrant VM to properly compile the test
programs. This can be done by adding the following line to the `Vagrantfile`:

```ruby
config.vm.synced_folder "./c11-race-detection/", "/home/vagrant/c11-race-detection"
```

## Compiling

Compiling the code should be done inside the Vagrant VM. Compiling a specific
test can be done with

```sh
./run.sh [test_name]
```

If the test name is omitted, all tests will be built.

This will compile the specified program and run it while writing traces into
the `traces/` folder.

From _outside_ of Vagrant, the Python script can run for analysis of the traces
it will print any data races it finds.

```sh
./main.py [test_name]
```

Again, if the test name is omitted, all tests are analyzed.

## Acknowledgements

The c11tester tool is created by Weiyu Luo, Brian Norris, and Brian Demsky and
distributed under the GPLv2 license.

