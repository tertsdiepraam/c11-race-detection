#!/usr/bin/env python
"""
Simply calls the parser and the analysis in succession for the specified test case.
"""
import parser
import analysis

parser.main()
analysis.main()
