"""
This script parses the output of c11tester, which should be in the format:
`traces/[test name].txt` and parses it into several csv files:
`traces/[test name]_0.csv`, `traces/[test name]_1.csv`, etc.

Any information outside the execution trace is discarded. Generally, this
script should not be run on its own, but is invoked by `main.py`.
"""
import csv
import sys
import glob
from pathlib import Path
from typing import Iterator, List


DIR = Path("traces")

def parse_line(line: str) -> List[str]:
    parts = line.split("  ")
    parts = [p.strip() for p in parts if p]
    index = 0
    for i, p in enumerate(parts):
        if p.startswith("("):
            index = i
            break
    head = parts[:index]
    tail = "".join(parts[index:]).replace(" ", "")
    if len(head) < 7:
        head = head + ([""] * (7-len(head)))
    head.append(tail)
    return head

def write_trace(name: Path, lines: Iterator[str], i: int) -> bool:
    line = next(lines)
    while "---------------" not in line:
        if line is None:
            return False
        line = next(lines)
    
    # Skip header
    next(lines)
    next(lines)
    
    filename = name.with_name(f"{name.stem}_{i}.csv")
    print(f"Writing {filename}")
    with open(filename, "w") as f:

        w = csv.writer(f)
        w.writerow(['#', 't', 'Action type', 'MO', 'Location', 'Value', 'Rf', 'CV'])
        
        line = next(lines)
        while not line.startswith("HASH"):
            w.writerow(parse_line(line))
            line = next(lines)
        
        # Eat final "---------"
        line = next(lines)

    return True

def parse(filename: Path):
    infile = open(filename)
    lines = iter(infile.readlines())

    i = 0
    try:
        while True:
            write_trace(filename, lines, i)
            i += 1
    except StopIteration:
        print("Done")

    infile.close()

def main():
    print(sys.argv)
    if len(sys.argv) > 1:
        parse(DIR / f"{sys.argv[1]}.txt")
    else:
        for filename in DIR.glob("*.txt"):
            print(f"Parsing: {filename}")
            parse(filename)

if __name__ == "__main__":
    main()
